# ads_cons_alg
***************************
Repositorio das Aulas e Exercicios da disciplina de Construcao de Algoritmos.

Comandos basicos para manipular seu repositorio 

1) Clonar um repositorio : git clone https://www.apenasumexemplo.com.br/arquivo.git (URL do repositorio)

2) Atualizar repositorio : git pull

3) Enviar novos arquivos : 
    
	3.1) git add .
	3.2) git commit -m "Mensagem a sua escolha"
	3.3) git push 

4) Mais sobre o Git, assitir o vídeo : https://www.youtube.com/watch?v=tf59zTUT6Sg